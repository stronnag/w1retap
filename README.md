## w1retap - One wire sensor logging.

w1retap is a system for logging data from a 1-wire weather sensors to either a relational database or files (or combination thereof).

w1retap supports any (reasonable) number of following sensors from [AAG Electrónica (AAG)](http://www.aagelectronica.com/), based on [Dallas Semiconductors](http://www.dalsemi.com/) devices.

* TAI8520 (DS1820 / DS18S20) Temperature sensors;
* TAI8540B (DS2438) Humidity Sensor;
* TAI8570 Pressure Sensor;
* TAI8575 Rain Gauge;
* TAI8515 Weather Station (DS2423,DS18S20,DS2450);
* DS2409 Microlan coupler;
* DS2490 USB adaptor;
* DS2480 Serial adaptor.

A number of other sensors, typically "hobby/build your own", are supported:

* [SHT11 based humidity sensor](http://home.hetnet.nl/~thomas_7/1Wire/1-WireIOPort.html)
* [MPX4115A based pressure sensor](http://home.comcast.net/~andrew.g.miller/barometer/) ('fronted' by DS2438).

w1retap is designed to run on the Linux operating system and assumes that the interface between the computer and the 1-wire system is either a DS2490 USB adaptor (aka DS9490R) or a DS2480 RS232 serial adaptor (aka DS9097U-S09). w1retap also runs on FreeBSD.

Logging is performed by one or more "plugin" modules, these currently include:

Built in Modules:

* Text file
* CSV file

Optional Modules:

* Sqlite (version 3)
* PostgreSQL
* MariaDB (MySQL)
* ODBC
* XML file

Note: You only need to install development packages for the optional storage module(s) you intend to use.

Note that for MySQL compatible storage, you need to install one of the MariaDB or MySQL development packages. w1retap meson will detect the installed development package.

### Files:

* src: Contains the w1retap software.
* src/libusblinux300: The Dallas PD 1-wire SDK (modified to be 64 bit clean and support modern compilers)
* doc: Documentation on configuring and using w1retap

## Installation

* Development packages for the required storage backend (postgresql / sqlite3 / mariadb (mysql) / odbc / xml).

* `libusb-1.0`
* `meson` and `ninja`
* `glib` / `gio`
* C compiler (`gcc` or `clang`)

Installation may either be to a user directory or system wide:

```
# user local
meson setup _build  --buildtype=release --strip --prefix=~/.local
# system wide (/usr/bin etc).
meson setup _ubuild  --buildtype=release --strip --prefix=/usr/
# system wide (/usr/local/bin etc).
meson setup _ubuild  --buildtype=release --strip --prefix=/usr/local
```

Choose which ever suits; the user local build avoids the need for `sudo` / `doas`.

**You may ignore meson "not found" messages for unwanted storage components.**

```
# user local build
ninja -C _build
#or
# system build
ninja -C _ubuild
```

```
# user local install
ninja -C _build install
#or
# system install
sudo ninja -C _ubuild install
```

User local / install can be done in one step

```
ninja -C _build  install
```

### Installation paths

* The applications `w1retap` and `w1find` are installed to `$prefix/bin/`
* The project shared libraries are installed to `$prefix/lib/w1retap/` and the above executables have appropriate `RUNPATH` set.

The demonstration / test programs in `src/libusblinux300` are not installed and their `RUNPATH` is set to `$ORIGIN` which means they can be run in-situ (e.g. from the build directory `_build/src/libusblinux300/`). These programs are not installed by default; if it is required to install them:

* Copy the executable from `_build/src/libusblinux300/` to the required directory, e.g.:

```
  install _build/src/libusblinux300/coupler ~/.local/bin/
```
* Set `RUNPATH`, e.g:

```
  patchelf --set-rpath '$ORIGIN/../lib/w1retap/' ~/.local/bin/coupler`
```
Note that Debian and derivatives may have added an 'arch' path (e.g. `../lib/x86_64-linux-gnu/w1retap/`

(c) Jonathan Hudson

Unless otherwise indicated, the package is released under the GNU Public Licence.
