#!/usr/bin/ruby
# -*- coding: utf-8 -*-

# Update record to convert HB Solar mV to W/m^2 and Lux values and add to document
# format PGSQL. See also <https://xkcd.com/327/>

ARGF.each do |l|
  l = l.force_encoding("utf-8") rescue l
  l.chomp!
  stamp,name,value,units,timet = l.split(/\t/)
  if name  == 'SOLAR'
    val =  value.to_f
    wpm2 = val * 2.9682
    lux = wpm2 / 0.0079
    cmd = "update readings set wxdata['UVWPMSQ']='#{wpm2}', wxdata['UVLUX'] = '#{lux}'  where date='#{stamp}';"
    system "psql wx -c \"#{cmd}\""
    break
  end
end
