/*
 * Copyright (C) 2005 Jonathan Hudson <jh+w1retap@daria.co.uk>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>

#define W1_ROC (1 << 0)
#define W1_RMIN (1 << 1)
#define W1_RMAX (1 << 2)

#define ALLOCDEV 8
#define TBUF_SZ 32
#define MAXDLL 16

typedef struct w1_devlist w1_devlist_t;

typedef struct {
  char *abbrv;
  char *name;
  char *units;
  float value;
  short valid;
  short flags;
  float rmin;
  float rmax;
  float roc;
  float lval;
  time_t ltime;
} w1_sensor_t;

typedef struct {
  char *serial;
  char *devtype;
  short init;
  w1_sensor_t s[2];
} w1_device_t;

struct w1_devlist {
  int numdev;
  int ndll;
  int delay;
  int portnum;
  char *iface;
  char *rcfile;
  char *repfile;
  time_t logtime;
  short verbose;
  short daemonise;
  short logtmp;
  short doread;
  w1_device_t *devs;
};

static int w1_validate(w1_devlist_t *w1, w1_sensor_t *s) {
  int chk = 0;

  s->valid = 1;

  if ((s->flags & W1_RMIN) && (s->value < s->rmin)) {
    s->value = (s->ltime) ? s->lval : s->rmin;
    chk = 1;
  }
  if ((s->flags & W1_RMAX) && (s->value > s->rmax)) {
    s->value = (s->ltime) ? s->lval : s->rmax;
    chk = 1;
  }

  if (chk == 0 && (s->flags & W1_ROC)) {
    if (s->ltime > 0 && s->ltime != w1->logtime) {
      float rate = fabs(s->value - s->lval) * 60.0 / (w1->logtime - s->ltime);
      if (rate > s->roc) {
        s->valid = 0;
        puts("Range Fails");

#if 1
        if (w1->repfile) {
          FILE *rfp;
          if (NULL != (rfp = fopen(w1->repfile, "a"))) {
            char buf[80];
            logtimes(w1->logtime, buf);
            fprintf(rfp, "%s %s %.2f %.2f %.2f %d %d\n", buf, s->abbrv, s->value, rate, s->lval, s->ltime, w1->logtime);
            fclose(rfp);
          }
        }
#endif
        s->value = s->lval;
      }
    }
    if (s->valid) {
      s->ltime = w1->logtime;
      s->lval = s->value;
    }
  }

  (int)s->valid;
}

int main(void) {
  w1_devlist_t w = {0}, *w1;
  w1_device_t devs[1] = {0};
  float tvals[] = {1013, 1013.5, 700, 700, 700, 1014};

  w1 = &w;
  w1->numdev = 1;
  w1->devs = devs;
  w1->devs[0].init = 1;
  w1->devs[0].s[0].flags = 7;
  w1->devs[0].s[0].roc = 100;
  w1->devs[0].s[0].rmin = 800;
  w1->devs[0].s[0].rmax = 1200;

  int n;
  time_t tt = time(0);

  for (n = 0; n < sizeof(tvals) / sizeof(float); n++) {
    w1->devs[0].s[0].value = tvals[n];
    w1->logtime = tt + n * 120;
    w1_validate(w1, &w1->devs[0].s[0]);
    printf("%d: Test: %.1f Value: %.1f valid: %d  lval: %.1f"
           " tim: %d ltim %d\n",
           n, tvals[n], w1->devs[0].s[0].value, w1->devs[0].s[0].valid, w1->devs[0].s[0].lval, w1->logtime,
           w1->devs[0].s[0].ltime);
  }
}
